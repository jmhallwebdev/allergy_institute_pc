var gulp = require('gulp'),
postcss = require('gulp-postcss'),
autoprefixer = require('autoprefixer'),
cssvars = require('postcss-simple-vars'),
nested = require('postcss-nested'),
cssimport = require('postcss-import');

//copy app/styles/styles.css to temp/styles/styles.css, 
//first run it through css-simplevars, nested & autoprefixer, 
//which give us variables and autoprefixes via npm addons to 
//postcss
gulp.task('styles', function() {
	return gulp.src('./app/assets/styles/styles.css')
		.pipe(postcss([cssimport, cssvars, nested, autoprefixer]))
		.pipe(gulp.dest('./app/temp/styles'));
});