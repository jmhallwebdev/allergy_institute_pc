var gulp = require('gulp'),
watch = require('gulp-watch'),
browserSync = require('browser-sync').create();

gulp.task('watch', function() {

	//starts browserSync and has it look in the app folder
	//and auto refresh
	browserSync.init({
		notify: false,  //removes notify box
		server: {
			baseDir: "app"
		}
	});

	//when index.html is saved have browserSync reload
	watch('./app/index.html', function() {
		browserSync.reload();
	});

	//when any .css file is saved run cssInject
	watch('./app/assets/styles/**/*.css', function() {
		gulp.start('cssInject');
	});
});

//reload the css when browserSync reloads the page,
//requiring the styles task as a dependency first (see above)
gulp.task('cssInject', ['styles'], function() {
	return gulp.src('./app/temp/styles/styles.css')
		.pipe(browserSync.stream());
});